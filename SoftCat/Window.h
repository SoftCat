#include <string>
#include <iostream>

#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include <GL/gl.h>
#include <GL/glu.h>

using std::string;

namespace SC
{
	class Window
	{
		private:
			int m_Width;
			int m_Height;
			int m_Bpp;
			bool m_Fullscreen;
			string m_Title;

		public:
			Window();
			Window(int width, int height, int bpp, bool fullscreen, const string& title);
			~Window();

			void setSize(int width, int height);
			int getHeight();
			int getWidth();
	};
};
