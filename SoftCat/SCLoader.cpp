#include "SCLoader.h"

namespace SC {
    SCLoader::SCLoader() {
    // initialize the video subsystem
    if ( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        //TODO: BETTER ERROR HANDLING?
        printf("Unable to initialize SDL: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
        }
    }

    SCLoader::~SCLoader() {
        SDL_Quit();
    }
};

