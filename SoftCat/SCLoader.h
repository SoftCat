#include <SDL/SDL.h>
/*
* OpenGL wrapper header, it provides portability.
*/
#include <SDL/SDL_opengl.h>

namespace SC {
    class SCLoader {
        public:
                SCLoader();
                ~SCLoader();
    };
};
